#include "Entity.h"

#include "RenderManager.h"
#include "Game.h"

Entity::Entity()
{
	posX = 0;
	posY = 0;
	velX = 0;
	velY = 0;
	visible = true;
}

Entity::~Entity()
{
	tex.free();
}

/// <summary>
/// Initialise the new entity.
/// </summary>
/// <param name="imgPath">Path to the image used for this entity.</param>
/// <returns>Whether or not the initialisation was successful.</returns>
bool Entity::init(std::string imgPath)
{
	if (!tex.loadTexture(imgPath))
	{
		std::cout << "loadTexture failed\n";
		return false;
	}
	return true;
	setPosition(0, 0);
	setVelocity(0, 0);
}

/// <summary>
/// Update the entity's state.
/// </summary>
void Entity::update()
{
	RenderManager* renderManager = RenderManager::getInstance();
	Game* game = Game::getInstance();

	// Move the entity.
	posX += velX;
	posY += velY;

	// Bounce the entity off the edge of the screen if it's touching.
	if (posX < 0)
	{
		posX = 0;
		velX = -velX;
	}
	if ((posX + tex.getWidth()) > renderManager->getWidth())
	{
		posX = renderManager->getWidth() - tex.getWidth();
		velX = -velX;
	}
	if (posY < 0)
	{
		posY = 0;
		velY = -velY;
	}
	if ((posY + tex.getHeight()) > renderManager->getHeight())
	{
		posY = renderManager->getHeight() - tex.getHeight();
		velY = -velY;

		game->reset();
	}
}

/// <summary>
/// Render the entity.
/// </summary>
void Entity::render()
{
	if (visible) tex.render(posX, posY);
}

/// <summary>
/// Destroy the entity.
/// </summary>
void Entity::free()
{
	tex.free();
}

void Entity::setPosition(float x, float y)
{
	posX = x;
	posY = y;
}

void Entity::setVelocity(float x, float y)
{
	velX = x;
	velY = y;
}

float Entity::getPositionX()
{
	return posX;
}

float Entity::getPositionY()
{
	return posY;
}

float Entity::getVelocityX()
{
	return velX;
}

float Entity::getVelocityY()
{
	return velY;
}

int Entity::getWidth()
{
	return tex.getWidth();
}

int Entity::getHeight()
{
	return tex.getHeight();
}

/// <summary>
/// Check if this entity is colliding with the ball.
/// </summary>
/// <param name="ball">The ball entity.</param>
/// <returns>Whether or not the two are colliding.</returns>
bool Entity::checkBallCollision(Entity* ball)
{
	if (this == ball) return false; // can't collide with ourselves!

	// First detect if there's any collision at all
	SDL_Rect thisRect = { posX, posY, getWidth(), getHeight() };
	SDL_Rect thatRect = { ball->getPositionX(), ball->getPositionY(), ball->getWidth(), ball->getHeight() };
	SDL_Rect intersection;
	bool result = SDL_IntersectRect(&thisRect, &thatRect, &intersection);

	if (result)
	{
		// Now figure out which edge the collision was on (relative to this entity, not the ball)
		int top = abs(thisRect.y - (thatRect.y + thatRect.h));
		int bottom = abs((thisRect.y + thisRect.h) - (thatRect.y));
		int left = abs(thisRect.x - (thatRect.x + thatRect.w));
		int right = abs((thisRect.x + thisRect.w) - (thatRect.x));
		int minX = (left > right) ? right : left;
		int minY = (top > bottom) ? bottom : top;
		
		// Flip the velocity depending on which axis we're touching on
		if (minX < minY)
		{
			ball->setVelocity(-ball->getVelocityX(), ball->getVelocityY());
		}
		else
		{
			ball->setVelocity(ball->getVelocityX(), -ball->getVelocityY());
		}
	}

	return result;
}

void Entity::setVisible(bool b)
{
	visible = b;
}

bool Entity::isVisible()
{
	return visible;
}