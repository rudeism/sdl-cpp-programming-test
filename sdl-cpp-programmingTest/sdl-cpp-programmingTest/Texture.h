#ifndef Texture_h_
#define Texture_h_

#include <SDL.h>
#include <string>

class Texture
{
	public:
		Texture();
		~Texture();

		bool loadTexture(std::string path);
		void free();
		void render(int x, int y);

		int getWidth();
		int getHeight();
	protected:
		SDL_Texture* tex;
		int width;
		int height;
	private:
};

#endif

