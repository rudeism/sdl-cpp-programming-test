#include "Game.h"

#include "RenderManager.h"

Game* Game::instance = NULL;

// Return the game singleton.
Game* Game::getInstance()
{
	if (instance == NULL) instance = new Game();
	return instance;
}

// Constructor
Game::Game()
{
	running = false;
}

/// <summary>
/// Initialise the data structures for each game object.
/// </summary>
/// <returns> A bool showing (un)successful initialisation. </returns>
bool Game::init()
{
	RenderManager* renderManager = RenderManager::getInstance();

	// Create the ball.
	if (!ball.init("Resources/Sprites/Ball.bmp"))
	{
		return false;
	}
	ball.setPosition(renderManager->getWidth() / 2 - ball.getWidth() / 2, renderManager->getHeight() - BALL_START_Y);
	ball.setVelocity(6, -4);

	// Create the paddle.
	if (!paddle.init("Resources/Sprites/Paddle.bmp"))
	{
		return false;
	}
	paddle.setPosition(renderManager->getWidth() / 2 - paddle.getWidth() / 2, renderManager->getHeight() - BALL_START_Y + ball.getHeight() / 2 + paddle.getHeight()/2);

	// Calculate where to place the bricks, then create them.
	int offsetX = -1;
	int offsetY = -1;
	for (int i = 0; i < getBrickCount(); ++i)
	{
		if (!bricks[i].init("Resources/Sprites/Brick.bmp"))
		{
			return false;
		}
		int gridX = i % GRID_WIDTH;
		int gridY = i / GRID_WIDTH;
		if (offsetX == -1)
		{
			int totalWidth = (bricks[i].getWidth() * GRID_WIDTH) + (BRICK_GAP * (GRID_WIDTH - 1));
			offsetX = renderManager->getWidth() / 2 - totalWidth / 2;
			offsetY = TOP_GAP;
		}

		bricks[i].setPosition(offsetX + (bricks[i].getWidth() + BRICK_GAP) * gridX, offsetY + (bricks[i].getHeight() + BRICK_GAP) * gridY);
	}

	return true;
}

/// <summary>
/// Start the game.
/// </summary>
void Game::start()
{
	running = true;
}

/// <summary>
/// Update the game state.
/// </summary>
void Game::update()
{
	if (!running) return;

	ball.update();

	// Check for ball-brick collisions
	for (int i = 0; i < getBrickCount(); ++i)
	{
		Entity* brick = &bricks[i];
		if (brick->isVisible() && brick->checkBallCollision(&ball))
		{
			brick->setVisible(false);
			checkWinState();
			break;
		}
	}

	// Check for ball-paddle collision
	if (paddle.checkBallCollision(&ball))
	{
		// TODO: set velocity based on relative position to the paddle
	}
}

/// <summary>
/// Reset the game state.
/// </summary>
void Game::reset()
{
	RenderManager* renderManager = RenderManager::getInstance();

	for (int i = 0; i < getBrickCount(); ++i)
	{
		Entity* brick = &bricks[i];
		brick->setVisible(true);
	}
	ball.setPosition(renderManager->getWidth() / 2 - ball.getWidth() / 2, renderManager->getHeight() - BALL_START_Y);
	paddle.setPosition(renderManager->getWidth() / 2 - paddle.getWidth() / 2, renderManager->getHeight() - BALL_START_Y + ball.getHeight() / 2 + paddle.getHeight() / 2);

	running = false;
}

/// <summary>
/// Destroy & free all game objects.
/// </summary>
void Game::close()
{
	ball.free();
}

Entity* Game::getBall()
{
	return &ball;
}

Entity* Game::getPaddle()
{
	return &paddle;
}

int Game::getBrickCount()
{
	return GRID_WIDTH * GRID_HEIGHT;
}

Entity* Game::getBrick(int i)
{
	return &bricks[i];
}

/// <summary>
/// Set the position of the paddle (and the ball too, if the game hasn't started yet).
/// </summary>
/// <param name="x">the x position to place the paddle.</param>
void Game::setPaddlePos(int x)
{
	RenderManager* renderManager = RenderManager::getInstance();

	// Limit the x position before setting.
	int halfWidth = paddle.getWidth() / 2;
	if (x < halfWidth) x = halfWidth;
	if (x > (renderManager->getWidth() - halfWidth)) x = renderManager->getWidth() - halfWidth;
	paddle.setPosition(x - halfWidth, paddle.getPositionY());

	if (!isRunning())
	{
		ball.setPosition(x - ball.getWidth()/2, ball.getPositionY());
	}
}

bool Game::isRunning()
{
	return running;
}

/// <summary>
/// Check if all bricks have been destroyed.
/// Reset the game if so.
/// </summary>
void Game::checkWinState()
{
	for (int i = 0; i < getBrickCount(); ++i)
	{
		Entity* brick = &bricks[i];
		if (brick->isVisible()) return;
	}

	reset();
}