#include "Text.h"
#include <Windows.h>
#include "RenderManager.h"

Text::Text()
{
	font = NULL;
}

Text::~Text()
{

}

/// <summary>
/// Load a new string of text on screen, using an already-set font, size and colour.
/// </summary>
/// <param name="t">The string to display.</param>
/// <returns>Whether or not creation was successful.</returns>
bool Text::loadText(std::string t = "Text Field")
{
	RenderManager* renderManager = RenderManager::getInstance();

	free();

	// Create the font.
	font = TTF_OpenFont(FONT_PATH, FONT_SIZE);
	if (font == NULL)
	{
		std::cout << "Font " << FONT_PATH << " not found\n";
		return false;
	}

	// Create a surface out of the font and text.
	SDL_Surface* textSurface = TTF_RenderText_Solid(font, t.c_str(), TEXT_COLOR);
	if (textSurface == NULL)
	{
		std::cout << "Text surface failed to create\n";
		return false;
	}

	// Create a texture from the surface.
	tex = SDL_CreateTextureFromSurface(renderManager->getRenderer(), textSurface);
	if (tex == NULL)
	{
		std::cout << "Text texture failed to create\n";
		return false;
	}

	width = textSurface->w;
	height = textSurface->h;

	SDL_FreeSurface(textSurface);
	return true;
}

void Text::free()
{
	if (font != NULL)
	{
		TTF_CloseFont(font);
		font = NULL;
	}
	Texture::free();
}