#ifndef Game_h_
#define Game_h_

#include <iostream>

#include "Texture.h"
#include "Entity.h"

class Game
{
	public:
		static Game* getInstance();

		bool init();
		void start();
		void update();
		void reset();
		void close();

		Entity* getBall();
		Entity* getPaddle();
		int getBrickCount();
		Entity* getBrick(int i);
		void setPaddlePos(int x);
		bool isRunning();

	private:
		static Game* instance;
		Game();

		const static int GRID_WIDTH = 10;
		const static int GRID_HEIGHT = 5;
		const static int BRICK_GAP = 2;
		const static int TOP_GAP = 40;
		const static int BALL_START_Y = 80;

		Entity ball;
		Entity paddle;
		Entity bricks[GRID_WIDTH * GRID_HEIGHT];

		bool running;

		void checkWinState();
};

#endif