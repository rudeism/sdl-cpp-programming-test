#ifndef TimeManager_h_
#define TimeManager_h_

#include <SDL.h>

class TimeManager
{
public:
	static TimeManager* getInstance();

	void start();
	void stop();

	Uint32 getTicks();
	
	bool isStarted();
	void frameDelay();

protected:
	const int FPS = 60;
	const int TICKS_PER_FRAME = 1000 / FPS;

private:
	static TimeManager* instance;
	TimeManager();

	Uint32 ticks;
	bool started;
};

#endif