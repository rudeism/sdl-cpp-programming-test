#ifndef RenderManager_h_
#define RenderManager_h_

#include <SDL.h>
#include <iostream>
#include "Text.h"

class RenderManager
{
public:
	static RenderManager* getInstance();

	bool init();
	void render();
	void close();

	SDL_Window* getWindow();
	SDL_Renderer* getRenderer();

	int getWidth();
	int getHeight();

protected:
	SDL_Window* window;
	SDL_Renderer* renderer;
	const int SCREEN_WIDTH = 750;
	const int SCREEN_HEIGHT = 500;

	Text displayText;
	const int TEXT_Y = 350;

private:
	static RenderManager* instance;
	RenderManager();
};

#endif