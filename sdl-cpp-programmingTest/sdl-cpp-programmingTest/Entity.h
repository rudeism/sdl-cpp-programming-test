#ifndef Entity_h_
#define Entity_h_

#include "Texture.h"
#include <string>

class Entity
{
public:
	Entity();
	~Entity();

	bool init(std::string imgPath);
	void update();
	void render();
	void free();

	void setPosition(float x, float y);
	void setVelocity(float x, float y);
	float getPositionX();
	float getPositionY();
	float getVelocityX();
	float getVelocityY();
	int getWidth();
	int getHeight();

	bool checkBallCollision(Entity* ball);

	void setVisible(bool b);
	bool isVisible();

private:
	Texture tex;
	float posX, posY;
	float velX, velY;
	bool visible;
};

#endif