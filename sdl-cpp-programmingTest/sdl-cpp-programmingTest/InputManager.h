#ifndef InputManager_h_
#define InputManager_h_

#include <SDL.h>

class InputManager
{
public:
	static InputManager* getInstance();

	void update();
	bool isTimeToQuit();

protected:
	SDL_Event e;
	SDL_Point mousePos;
	bool quit = false;

private:
	static InputManager* instance;
	InputManager();
};

#endif