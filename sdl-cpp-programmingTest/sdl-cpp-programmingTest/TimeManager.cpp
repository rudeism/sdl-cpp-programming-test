#include "TimeManager.h"

TimeManager* TimeManager::instance = NULL;

// Return the singleton class.
TimeManager* TimeManager::getInstance()
{
	if (instance == NULL) instance = new TimeManager();
	return instance;
}

TimeManager::TimeManager()
{
	ticks = 0;
	started = false;
}

/// <summary>
/// Start tracking time from this moment.
/// </summary>
void TimeManager::start()
{
	started = true;
	ticks = SDL_GetTicks();
}

/// <summary>
/// Stop tracking time.
/// </summary>
void TimeManager::stop()
{
	started = false;
	ticks = 0;
}

/// <summary>
/// Get the time passed since starting.
/// </summary>
/// <returns>The time passed since starting</returns>
Uint32 TimeManager::getTicks()
{
	Uint32 t = 0;
	if (started)
	{
		t = SDL_GetTicks() - ticks;
	}
	return t;
}

bool TimeManager::isStarted()
{
	return started;
}

/// <summary>
/// Add an artificial delay to ensure a smooth frame rate.
/// </summary>
void TimeManager::frameDelay()
{
	Uint32 frameTicks = getTicks();
	if (frameTicks < TICKS_PER_FRAME)
	{
		SDL_Delay(TICKS_PER_FRAME - frameTicks);
	}
}