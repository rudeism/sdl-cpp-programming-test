#include <SDL.h>
#include <stdio.h>
#include <iostream>

#include "InputManager.h"
#include "RenderManager.h"
#include "TimeManager.h"

#include "Game.h"

bool quit = false;
SDL_Event e;

InputManager* inputManager;
RenderManager* renderManager;
TimeManager* timeManager;
Game* game;

bool init()
{
	bool result = true;

	inputManager = InputManager::getInstance();
	renderManager = RenderManager::getInstance();
	result &= renderManager->init();

	timeManager = TimeManager::getInstance();
	timeManager->start();

	game = Game::getInstance();
	result &= game->init();

	return result;
}

void close()
{
	game->close();
	renderManager->close();
}

int main( int argc, char* args[] )
{
	bool initSuccess = init();
	if (!initSuccess) return 0;

	//Update the surface
	SDL_UpdateWindowSurface(renderManager->getWindow() );

	while (!inputManager->isTimeToQuit())
	{
		timeManager->start();
		inputManager->update();
		game->update();
		renderManager->render();
		timeManager->frameDelay();
	}
	close();

	return 0;
}