#include "InputManager.h"

#include "Game.h"

InputManager* InputManager::instance = NULL;

// Return the singleton class.
InputManager* InputManager::getInstance()
{
	if (instance == NULL) instance = new InputManager();
	return instance;
}

InputManager::InputManager()
{
	quit = false;
}

/// <summary>
/// Check all inputs and modify the game state accordingly.
/// </summary>
void InputManager::update()
{
	Game* game = Game::getInstance();
	while (SDL_PollEvent(&e) != 0)
	{
		if (e.type == SDL_QUIT)
		{
			quit = true;
		}
		else if (e.type == SDL_MOUSEMOTION)
		{
				int x, y;
				SDL_GetMouseState(&x, &y);
				game->setPaddlePos(x);
		}
	}

	const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
	if (currentKeyStates[SDL_SCANCODE_SPACE])
	{
		game->start();
	}
	else if (currentKeyStates[SDL_SCANCODE_ESCAPE])
	{
		quit = true;
	}
}

// Return a flag signifying if the game should exit.
bool InputManager::isTimeToQuit()
{
	return quit;
}