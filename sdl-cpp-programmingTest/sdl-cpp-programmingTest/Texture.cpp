#include "Texture.h"

#include "RenderManager.h"

Texture::Texture()
{
	tex = NULL;
	width = 0;
	height = 0;
}

Texture::~Texture()
{
	free();
}

/// <summary>
/// Load a new texture.
/// </summary>
/// <param name="path">The location of the BMP file to be loaded.</param>
/// <returns>A bool showing if the load was successful.</returns>
bool Texture::loadTexture(std::string path)
{
	RenderManager* renderManager = RenderManager::getInstance();

	free();

	SDL_Surface* sfc = SDL_LoadBMP(path.c_str());
	if (sfc != NULL)
	{
		tex = SDL_CreateTextureFromSurface(renderManager->getRenderer(), sfc);
		if (tex != NULL)
		{
			width = sfc->w;
			height = sfc->h;
		}
		SDL_FreeSurface(sfc);
	}
	return tex != NULL;
}

void Texture::free()
{
	if (tex != NULL)
	{
		SDL_DestroyTexture(tex);
		tex = NULL;
		width = 0;
		height = 0;
	}
}

/// <summary>
/// Render the texture.
/// </summary>
/// <param name="x">The X position of the render on screen.</param>
/// <param name="y">The Y position of the render on screen.</param>
void Texture::render(int x, int y)
{
	RenderManager* renderManager = RenderManager::getInstance();
	SDL_Rect quad = { x, y, width, height };
	SDL_RenderCopy(renderManager->getRenderer(), tex, NULL, &quad);
}

int Texture::getWidth()
{
	return width;
}

int Texture::getHeight()
{
	return height;
}