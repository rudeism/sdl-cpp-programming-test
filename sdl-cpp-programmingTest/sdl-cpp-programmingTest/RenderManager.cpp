#include "RenderManager.h"

#include "Game.h"

RenderManager* RenderManager::instance = NULL;

// Return the singleton class.
RenderManager* RenderManager::getInstance()
{
	if (instance == NULL) instance = new RenderManager();
	return instance;
}

RenderManager::RenderManager()
{
	window = NULL;
	renderer = NULL;
}

/// <summary>
/// Initialise the SDL window and renderer.
/// </summary>
/// <returns> A bool if initialisation was (un)successful.</returns>
bool RenderManager::init()
{
	// Error check that SDL and TTF are initialised correctly
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL_Init failed\n";
		return false;
	}
	if (TTF_Init() == -1)
	{
		std::cout << "TTF_Init failed: " << TTF_GetError() << "\n";
		return false;
	}

	// Create the game window
	window = SDL_CreateWindow("Rudeism's Breakout", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (window == NULL)
	{
		std::cout << "SDL_CreateWindow failed\n";
		return false;
	}

	// Create the renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (renderer == NULL)
	{
		std::cout << "SDL_CreateRenderer failed\n";
		return false;
	}
	SDL_SetRenderDrawColor(renderer, 0x8b, 0xac, 0x0f, 0xff);

	// Create the display prompt
	if (!displayText.loadText("PRESS SPACE TO START"))
	{
		return false;
	}
	return true;
}

/// <summary>
/// Render the game state.
/// </summary>
void RenderManager::render()
{
	Game* game = Game::getInstance();

	SDL_SetRenderDrawColor(renderer, 0x8b, 0xac, 0x0f, 0xFF);
	SDL_RenderClear(renderer);

	Entity* ball = game->getBall();
	ball->render();

	Entity* paddle = game->getPaddle();
	paddle->render();

	int brickCount = game->getBrickCount();
	for (int i = 0; i < brickCount; ++i)
	{
		Entity* brick = game->getBrick(i);
		brick->render();
	}

	// Only show the display prompt if the game hasn't started yet
	if (!game->isRunning()) displayText.render(SCREEN_WIDTH/2 - displayText.getWidth()/2, TEXT_Y);

	SDL_RenderPresent(renderer);
}

/// <summary>
/// Destroy the renderer, window and quit SDL.
/// </summary>
void RenderManager::close()
{
	displayText.free();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = NULL;
	renderer = NULL;

	TTF_Quit();
	SDL_Quit();
}

// Return the SDL_Window.
SDL_Window* RenderManager::getWindow()
{
	return window;
}

// Return the SDL_Renderer.
SDL_Renderer* RenderManager::getRenderer()
{
	return renderer;
}

int RenderManager::getWidth()
{
	return SCREEN_WIDTH;
}

int RenderManager::getHeight()
{
	return SCREEN_HEIGHT;
}