#ifndef Text_h_
#define Text_h_

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include "Texture.h"

class Text : public Texture
{
public:
	Text();
	~Text();

	void free();
	bool loadText(std::string t);
private:
	const char* FONT_PATH = "Resources/Fonts/PokemonGb-RAeo.ttf";
	const int FONT_SIZE = 28;
	const SDL_Color TEXT_COLOR = { 48, 98, 48 };
	TTF_Font* font;
};

#endif